﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Timers;
using System.Linq;
using TwitchBot.Events;
using TwitchBot.Enum;

namespace TwitchBot.Model
{
    public delegate void UserEventHandler(object s, TwitchUserEventArgs e);
    public delegate void MessageEventHandler(object s, TwitchMessageEventArgs e);

    [Serializable]
    public partial class TwitchChannel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string ChannelName { get; set; }
        public List<TwitchUser> UsersInChannel { get; set; }

        public Queue<string> OutgoingMessages = new Queue<string>();

        public bool IsAutoJoin { get; set; }

        public bool IsUserOnline { get; set; }

        public bool IsJoinedChannel { get; set; }

        public bool IsWaitForColorChange { get; private set; } = false;

        public bool Active { get; set; }

        public DateTime TimeStampCreated { get; set; }

        public DateTime TimeStampModified { get; set; }


        private const int COLOR_CHANGE_DELAY_MS = 1800;
        private const int LEAVE_ROOM_DELAY_MS = 500;

        private ITwitchIrcClient _twitchIrcClient;

        private Timer _utilityTimer;

        public event UserEventHandler UserEvent;
        public event MessageEventHandler MessageEvent;

        public TwitchChannel(ITwitchIrcClient twitchIrcClient)
        {
            _twitchIrcClient = twitchIrcClient;
        }

        public void LeaveChannel()
        {
            _utilityTimer = new Timer(LEAVE_ROOM_DELAY_MS);
            _utilityTimer.Elapsed += new ElapsedEventHandler(LeaveChannelSendReq);
            _utilityTimer.Enabled = true;
        }
        public void AddUserToChannel(TwitchUser user)
        {
            if (user == null) { return; }
            if (!UsersInChannel.Any(s => s.ID == user.ID))
            {
                UsersInChannel.Add(user);

                if (UserEvent != null)
                {
                    UserEvent(this, new TwitchUserEventArgs() { TwitchUser = user, MessageType = MessageType.Join });
                }
            }
        }
        public void RemoveUserFromChannel(TwitchUser user)
        {
            if (UsersInChannel.Any(s => s.ID == user.ID))
            {
                TwitchUser userInChannel = UsersInChannel.First(a => a.ID == user.ID);
                UsersInChannel.Remove(userInChannel);

                if (UserEvent != null)
                {
                    UserEvent(this, new TwitchUserEventArgs() { TwitchUser = user, MessageType = MessageType.Leave });
                }
            }
        }
        public void IsColorChange()
        {
            IsWaitForColorChange = true;

            _utilityTimer = new Timer(COLOR_CHANGE_DELAY_MS);
            _utilityTimer.Elapsed += new ElapsedEventHandler(ColorChangeAck);
            _utilityTimer.Enabled = true;
        }
        public string ColorChangeString(ChatTextColor chatTextColor)
        {
            switch (chatTextColor)
            {
                case ChatTextColor.Blue:
                    return "DodgerBlue";
                case ChatTextColor.Green:
                    return "SpringGreen";
                case ChatTextColor.Purple:
                    return "BlueViolet";
                case ChatTextColor.Yellow:
                    return "GoldenRod";
                case ChatTextColor.Red:
                    return "Red";
                default:
                    return "";
            }
        }

        public void MessageReceived(TwitchMessage message)
        {
            if (message.IsColorChanged && IsWaitForColorChange)
            {
                _utilityTimer.Close();

                _utilityTimer = new Timer(COLOR_CHANGE_DELAY_MS);
                _utilityTimer.Elapsed += new ElapsedEventHandler(ColorChangeAck);
                _utilityTimer.Enabled = true;
            }
            else
            {
                if (MessageEvent != null)
                {
                    MessageEvent(this, new TwitchMessageEventArgs() { TwitchMessage = message });
                }
            }
        }
        public bool IsUserInChannel(string userName)
        {
            if (UsersInChannel.Any(s => s.UserName == userName.ToLower()))
                return true;
            return false;
        }

        private void LeaveChannelSendReq(object sender, System.EventArgs e)
        {
            _utilityTimer.Close();
            _twitchIrcClient.SendMessage("PART #" + ChannelName);
            IsJoinedChannel = false;
        }
        private void ColorChangeAck(object sender, System.EventArgs e)
        {
            _utilityTimer.Close();
            IsWaitForColorChange = false;

            while (OutgoingMessages.Count > 0)
            {
                string _outgoingMessage = OutgoingMessages.Dequeue();
                _twitchIrcClient.SendMessage(_outgoingMessage);
            }
        }
    }
}