﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TwitchBot.Model
{
    [Serializable]
    public partial class TwitchMessage
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int? Channel_ID { get; set; }

        public Guid? User_ID { get; set; }

        public MessageType MessageType { get; set; }

        public string MessageFormatted { get; set; }

        public string MessageRaw { get; set; }

        public DateTime TimeStamp { get; set; }

        public virtual TwitchChannel Channel { get; set; }

        public virtual TwitchUser User { get; set; }


        private string _userName;
        private string _channelName;
        private bool _isColorChange = false;

        public TwitchMessage(string messageRaw)
        {
            MessageRaw = messageRaw;
            MessageFormatted = "";

            _userName = GetUserName(messageRaw);

            if (IsColorChange()) { return; }

            // Get username if available and assign
            if (!string.IsNullOrWhiteSpace(_userName))
            {
                string _validIrcString = ":" + _userName + "!" + _userName + "@" + _userName + ".tmi.twitch.tv ";

                // Validation
                if (_validIrcString.Length < MessageRaw.Length)
                {
                    string msgSubstring = MessageRaw.Substring(_validIrcString.Length);

                    if (msgSubstring.StartsWith("WHISPER"))
                        MessageType = MessageType.Whisper;
                    else if (msgSubstring.StartsWith("USERSTATE"))
                        MessageType = MessageType.Notice;
                    else if (msgSubstring.StartsWith("JOIN"))
                        MessageType = MessageType.Join;
                    else if (msgSubstring.StartsWith("PART"))
                        MessageType = MessageType.Leave;
                    else if (msgSubstring.StartsWith("PRIVMSG"))
                        MessageType = MessageType.Chat;
                    else
                        MessageType = MessageType.Other;

                    switch (MessageType)
                    {
                        case MessageType.Chat:
                            AssignProperties(msgSubstring);
                            break;
                        case MessageType.Join:
                            AssignProperties(msgSubstring);
                            break;
                        case MessageType.Leave:
                            AssignProperties(msgSubstring);
                            break;
                        case MessageType.Whisper:
                            MessageFormatted = msgSubstring.Substring(msgSubstring.IndexOf(":") + 1);
                            break;
                        case MessageType.Other:
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void AssignProperties(string msgSubstring)
        {
            int indexToAction = msgSubstring.IndexOf("#");
            string strMessageAndChannel = msgSubstring.Substring(indexToAction);
            int countToColon = strMessageAndChannel.IndexOf(":");

            if (countToColon > 0)
            {
                _channelName = strMessageAndChannel.Substring(1, countToColon - 2);
                MessageFormatted = strMessageAndChannel.Substring(countToColon + 1);
            }
            else
                _channelName = strMessageAndChannel.Substring(1);
        }
        private string GetUserName(string message)
        {
            int countToExclamation = 0;

            if (!string.IsNullOrWhiteSpace(message))
            {
                if (message.IndexOf("!") > 0)
                    countToExclamation = message.IndexOf("!") - 1;

                if (countToExclamation != 0)
                {
                    return message.Substring(1, countToExclamation);
                }
                else
                    return null;
            }
            else
                return null;

        }

        private bool IsColorChange()
        {
            if (string.IsNullOrWhiteSpace(MessageRaw)) { return false; }

            string _tempChannelName = "";
            string _substring1 = MessageRaw.Substring(1);
            int _indexToColon = _substring1.IndexOf(":");
            int _indexToHash = _substring1.IndexOf("#");

            if (_indexToColon != -1 && _indexToHash != -1)
                _tempChannelName = _substring1.Substring(_substring1.IndexOf("#") + 1, _indexToColon - _substring1.IndexOf("#") - 2);

            StringBuilder _sb = new StringBuilder();
            _sb.AppendFormat(":tmi.twitch.tv NOTICE #{0} :Your color has been changed.", _tempChannelName);

            if (MessageRaw.Contains(_sb.ToString()))
            {
                _channelName = _tempChannelName;
                _isColorChange = true;
                return true;
            }
            return false;
        }

        public string UserName
        {
            get
            {
                return _userName;
            }
        }
        public string ChannelName
        {
            get
            {
                return _channelName;
            }
        }
        public bool IsColorChanged
        {
            get
            {
                return _isColorChange;
            }
        }

    }

    public enum MessageType
    {
        Join,
        Leave,
        Chat,
        Whisper,
        Notice,
        Other
    }
}
