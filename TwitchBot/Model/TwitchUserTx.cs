﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwitchBot.Model
{
    public partial class TwitchUsersTx
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public Guid User_ID { get; set; }

        public int Channel_ID { get; set; }

        public LoginType LoginType { get; set; }

        public DateTime TimeStamp { get; set; }

        public virtual TwitchUser User { get; set; }

        public virtual TwitchChannel Channel { get; set; }
    }

    public enum LoginType
    {
        Login,
        Logoff
    }
}
