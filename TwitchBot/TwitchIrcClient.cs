﻿namespace TwitchBot
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Net.Sockets;
    using System.Timers;
    using Events;
    using IrcClient;
    using Model;
    using System.Threading.Tasks;
    using System.Linq;
    using System.Threading;
    using Enum;

    public delegate void StateChangeEventHandler(object s, TwitchBotEventArgs e);
    public delegate void ChannelEventHandler(object s, TwitchChannelEventArgs e);

    public class TwitchIrcClient : IDisposable, ITwitchIrcClient
    {
        public List<TwitchChannel> Channels { get; private set; } = new List<TwitchChannel>();
        public BotState BotState { get; private set; }
        public IIrcClient IrcClient { get; private set; }

        public event StateChangeEventHandler BotStateChange;
        public event Model.MessageEventHandler MessageEvent;
        public event ChannelEventHandler ChannelEvent;

        private const string SERVER_NAME = "irc.chat.twitch.tv";
        private const int PORT = 6667;
        public StringBuilder _sb = new StringBuilder();

        public TwitchIrcClient()
        {
            IrcClient = new IrcClient.IrcClient();
            IrcClient.MessageEvent += async (s, e) =>
            {
                await OnMessageEvent(s, e);
            };
        }
        public void ConnectToTwitch(string userName, string password)
        {
            BotState = BotState.Connecting;

            if (BotStateChange != null)
            {
                BotStateChange(this, new TwitchBotEventArgs() { BotState = BotState });
            }

            IrcClient = new IrcClient.IrcClient();
            IrcClient.MessageEvent += async (s, e) =>
            {
                await OnMessageEvent(s, e);
            };

            IrcClient.Connect(userName, password, SERVER_NAME, PORT, true, MessageRate());

            if (IrcClient.IsConnected)
            {
                BotState = BotState.Connected;

                if (BotStateChange != null)
                {
                    BotStateChange(this, new TwitchBotEventArgs() { BotState = BotState });
                }
            }
            Command_SendCommandCapRequest();
            Command_SendCommandCapRequest();
        }
        public void DisconnectFromTwitch()
        {

            for (int i = 0; i < Channels.Count; i++)
            {
                DisconnectFromTwitchChannel(Channels[i].ChannelName);
            }

            Thread.Sleep(500);

            try
            {
                IrcClient.Disconnect();
            }
            catch (Exception)
            { };

            BotState = BotState.Idle;

            if (BotStateChange != null)
            {
                BotStateChange(this, new TwitchBotEventArgs() { BotState = BotState });
            }
        }
        public TwitchChannel ConnectToTwitchChannel(string channelName)
        {
            if (IrcClient != null && !string.IsNullOrWhiteSpace(channelName))
            {
                TwitchChannel channel = new TwitchChannel(this)
                {
                    ChannelName = channelName,
                    TimeStampCreated = DateTime.UtcNow,
                    TimeStampModified = DateTime.UtcNow
                };

                channel.IsJoinedChannel = true;
                channel.UsersInChannel = new List<TwitchUser>();

                SendMessage("JOIN #" + channel.ChannelName.ToLower());
                Channels.Add(channel);
                return channel;
            }
            return null;
        }
        public void DisconnectFromTwitchChannel(string channelName)
        {
            if (Channels.Any(s => s.ChannelName == channelName))
            {
                TwitchChannel twitchChannel = Channels.FirstOrDefault(s => s.ChannelName == channelName);

                IrcClient.SendMessage("PART #" + twitchChannel.ChannelName);
                twitchChannel.LeaveChannel();

                Channels.Remove(twitchChannel);

                if (ChannelEvent != null)
                {
                    ChannelEvent(this, new TwitchChannelEventArgs() { TwitchChannel = twitchChannel, LoginType = LoginType.Logoff });
                }

                twitchChannel.LeaveChannel();
            }
        }
        public void SendMessage(string message)
        {
            IrcClient.SendMessage(message);
        }
        public void SendChatMessage(TwitchChannel twitchChannel, string message)
        {
            _sb.AppendFormat(":{0}!{0}@{0}.tmi.twitch.tv PRIVMSG #{1} : {2}", IrcClient.IrcServerData.userName,
            twitchChannel.ChannelName, message);

            if (twitchChannel.IsWaitForColorChange)
                twitchChannel.OutgoingMessages.Enqueue(_sb.ToString());
            else
            {
                IrcClient.SendMessage(_sb.ToString());
            }

            _sb.Clear();
        }
        public void SendWhisper(TwitchChannel twitchChannel, string message, string userName)
        {
            _sb.AppendFormat(":{0}!{0}@{0}.tmi.twitch.tv PRIVMSG #{1} :/w {2} {3}", IrcClient.IrcServerData.userName,
                twitchChannel.ChannelName, userName, message);
            IrcClient.SendMessage(_sb.ToString());
        }
        public void SendAction(TwitchChannel twitchChannel, string message)
        {
            _sb.AppendFormat(":{0}!{0}@{0}.tmi.twitch.tv PRIVMSG #{1} :/me {2}", IrcClient.IrcServerData.userName,
                twitchChannel.ChannelName, message);

            if (twitchChannel.IsWaitForColorChange)
            {
                twitchChannel.OutgoingMessages.Enqueue(_sb.ToString());
            }
            else
            {
                IrcClient.SendMessage(_sb.ToString());
            }

            _sb.Clear();
        }
        public void ChangeTextColor(TwitchChannel twitchChannel, ChatTextColor chatTextColor)
        {
            if (IrcClient.IsConnected && twitchChannel.IsJoinedChannel && !twitchChannel.IsWaitForColorChange)
            {
                _sb.AppendFormat(":{0}!{0}@{0}.tmi.twitch.tv PRIVMSG #{1} :.color {2}", IrcClient.IrcServerData.userName,
                    twitchChannel.ChannelName, twitchChannel.ColorChangeString(chatTextColor));
                IrcClient.SendMessage(_sb.ToString());

                _sb.Clear();

                twitchChannel.IsColorChange();
            }
        }

        private async Task OnMessageEvent(object sender, IrcMessageEventArgs e)
        {
            TwitchMessage message = new TwitchMessage(e.MessageRaw);

            if (string.IsNullOrWhiteSpace(message.UserName) || string.IsNullOrWhiteSpace(message.ChannelName))
            {
                if (message.UserName == "tmi.twitch.tv 001 botpxhorror :Welcome, GLHF")
                {
                    await Task.FromResult<object>(null);
                }
                else
                {
                    message.User = new TwitchUser()
                    {
                        UserName = message.UserName,
                        TimeStamp = DateTime.UtcNow
                    };
                }
            }
            else
            {
                TwitchChannel _channel = Channels.FirstOrDefault(t => t.ChannelName == message.ChannelName);

                if (_channel == null)
                {
                    await Task.FromResult<object>(null);
                    return;
                }

                _channel.MessageReceived(message);
            }

            if (MessageEvent != null)
            {
                MessageEvent(sender, new TwitchMessageEventArgs() { TwitchMessage = message });
            }

            await Task.FromResult<object>(null);
        }
        /*
        private void ManageUsers()
        {

            TwitchUser user;

            switch (message.MessageType)
            {
                case MessageType.Chat:

                    if (!channel.IsUserInChannel(message.UserName))
                    {
                        message.User = new TwitchUser()
                        {
                            UserName = message.UserName,
                            TimeStamp = DateTime.UtcNow
                        };

                        message.Channel = channel;

                        lock (channel.UsersInChannel)
                        {
                            channel.AddUserToChannel(message.User);
                        }
                        break;
                    }

                    user = channel.UsersInChannel.FirstOrDefault(s => s.UserName == message.UserName);

                    if (user != null)
                    {
                        message.User = user;
                    }
                    message.Channel = channel;
                    break;
                case MessageType.Join:
                    if (!channel.IsUserInChannel(message.UserName))
                    {
                        message.User = new TwitchUser()
                        {
                            UserName = message.UserName,
                            TimeStamp = DateTime.UtcNow
                        };

                        message.Channel = channel;

                        lock (channel.UsersInChannel)
                        {
                            channel.AddUserToChannel(message.User);
                        }

                        break;
                    }

                    user = channel.UsersInChannel.FirstOrDefault(s => s.UserName == message.UserName);

                    if (user != null)
                    {
                        message.User = user;
                    }

                    message.Channel = channel;
                    break;
                case MessageType.Leave:
                    user = channel.UsersInChannel.FirstOrDefault(s => s.UserName == message.UserName);

                    if (user != null)
                    {
                        message.User = user;

                        lock (channel.UsersInChannel)
                        {
                            channel.RemoveUserFromChannel(user);
                        }

                        break;
                    }
                    break;
                default:
                    break;
            }

            return message;
        }
        */
        private float MessageRate()
        {
            return (1000f * 20f) / (30f * .9f);       // 20 messages in 30 sec if not in milliseconds with 10% buffer
        }
        private void PingPong()
        {
            SendMessage("PONG :tmi.twitch.tv");
        }
        private void Command_SendMembershipCapRequest()
        {
            // Send cap request between joining server and channel to track new followers
            // Courtesy of Devin_Brimer
            string _capRequest = "CAP REQ :twitch.tv/membership";
            SendMessage(_capRequest);
        }
        private void Command_SendCommandCapRequest()
        {
            string _capRequest = "CAP REQ :twitch.tv/commands";
            SendMessage(_capRequest);
        }

        public void Dispose()
        {
            if (IrcClient != null)
            {
                IrcClient.MessageEvent -= async (s, e) =>
                {
                    await OnMessageEvent(s, e);
                };
            }
        }

        public bool IsConnected
        {
            get
            {
                return IrcClient.IsConnected;
            }
        }
        public IrcServerData IrcServerData
        {
            get
            {
                return IrcClient.IrcServerData;
            }
        }
    }
}
