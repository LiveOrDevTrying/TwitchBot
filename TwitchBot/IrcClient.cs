﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Net.Sockets;
using System.Timers;
using TwitchBot.Events;

namespace TwitchBot.IrcClient
{

    public delegate void MessageEventHandler(object sender, IrcMessageEventArgs e);

    public struct IrcServerData
    {
        public string serverName;
        public int port;
        public string userName;
        public bool hasOutgoingTimer;
    }
    public class IrcClient : IIrcClient
    {
        private TcpClient _tcpClient;
        private StreamReader _streamReader;
        private StreamWriter _streamWriter;
        private Queue<string> _outgoingMessages;
        private IrcServerData _ircClientData;

        private bool _isRunning = false;
        private byte[] _constantBuffer = new byte[0];
        private Timer _timer = new Timer();

        private event MessageEventHandler _messageEvent;

        public void Connect(string userName, string password, string serverName, int port, bool hasOutgoingTimer, float messageRate)
        {
            _ircClientData = new IrcServerData
            {
                userName = userName,
                serverName = serverName,
                port = port,
                hasOutgoingTimer = hasOutgoingTimer
            };

            _outgoingMessages = new Queue<string>();

            try
            {
                _tcpClient = new TcpClient(_ircClientData.serverName, _ircClientData.port);
            }
            catch (SocketException e)
            {
                Console.WriteLine("Did not connect.");
                Console.WriteLine(e);
                return;
            }

            _streamReader = new StreamReader(_tcpClient.GetStream());
            _streamWriter = new StreamWriter(_tcpClient.GetStream());

            _streamWriter.WriteLine("PASS " + password);
            _streamWriter.WriteLine("NICK " + userName.ToLower());
            _streamWriter.WriteLine("USER " + userName + "8 * :" + userName);
            _streamWriter.Flush();

            if (_ircClientData.hasOutgoingTimer)
            {
                StartMessageOutgoingTimer(messageRate);
            }

            _isRunning = true;
            WaitForData();
        }
        public void Disconnect()
        {
            _isRunning = false;
            _timer.Stop();
            _streamReader.Close();
            _tcpClient.Close();
        }
        public void SendMessage(string message)
        {
            if (_ircClientData.hasOutgoingTimer)
            {
                _outgoingMessages.Enqueue(message);
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(message))
                {
                    _streamWriter.WriteLine(message);
                    _streamWriter.Flush();

                    if (_messageEvent != null)
                    {
                        _messageEvent(this, new IrcMessageEventArgs() { MessageRaw = message, SendOrReceive = SendOrReceive.Send });
                    }
                }
            }
        }

        // Message sender
        private void StartMessageOutgoingTimer(float messageRate)
        {
            _timer = new Timer(messageRate);
            _timer.Elapsed += new ElapsedEventHandler(OutgoingMessageTimer_Tick);
            _timer.Enabled = true;
        }
        private void OutgoingMessageTimer_Tick(object sender, System.EventArgs e)
        {
            if (_outgoingMessages.Count > 0)
            {
                string _message = _outgoingMessages.Dequeue();
                if (!string.IsNullOrWhiteSpace(_message))
                {
                    _streamWriter.WriteLine(_message);
                    _streamWriter.Flush();

                    if (_messageEvent != null)
                    {
                        _messageEvent(this, new IrcMessageEventArgs() { MessageRaw = _message, SendOrReceive = SendOrReceive.Send });
                    }
                }
            }
        }

        // Message receiver
        private void WaitForData()
        {
            try
            {
                byte[] buffer = new byte[_tcpClient.ReceiveBufferSize];
                _tcpClient.GetStream().BeginRead(buffer, 0, buffer.Length, ReadData, buffer);
            }
            catch (Exception)
            {
            }
        }
        private void ReadData(IAsyncResult ar)
        {
            if (_isRunning)
            {
                int read = _tcpClient.GetStream().EndRead(ar);
                if (read == 0)
                {
                }
                else
                {
                    byte[] buffer = ar.AsyncState as byte[];
                    _constantBuffer = Combine(_constantBuffer, buffer, read);
                    string testString = Encoding.Default.GetString(_constantBuffer);
                    string testStringCopy = testString;
                    string toRemove = "";
                    while (true)
                    {
                        if (testString.Contains("\r\n"))
                        {
                            string newLine = testString.Substring(0, testString.IndexOf("\r\n"));

                            if (_messageEvent != null)
                            {
                                _messageEvent(this, new IrcMessageEventArgs() { MessageRaw = newLine, SendOrReceive = SendOrReceive.Receive });
                            }

                            toRemove += newLine + "\r\n";
                            testString = testString.Remove(0, testString.IndexOf("\r\n") + 2);

                        }
                        else
                        {
                            //testString no longer contains \r\n
                            string leftString = testStringCopy.Remove(0, toRemove.Length);
                            byte[] newLength = new byte[leftString.Length];
                            byte[] toRemoveBytes = Encoding.Default.GetBytes(toRemove);
                            Buffer.BlockCopy(_constantBuffer, toRemoveBytes.Length, newLength, 0, _constantBuffer.Length - toRemoveBytes.Length);
                            _constantBuffer = newLength;
                            break;
                        }
                    }
                    WaitForData();
                }
            }
        }
        private byte[] Combine(byte[] one, byte[] two, int twoRead)
        {
            byte[] ret = new byte[one.Length + twoRead];
            Buffer.BlockCopy(one, 0, ret, 0, one.Length);
            Buffer.BlockCopy(two, 0, ret, one.Length, twoRead);
            return ret;
        }
        private byte[] Combine(byte[] Prepend, byte[] Message)
        {
            byte[] returnByte = new byte[Prepend.Length + Message.Length];
            Buffer.BlockCopy(Prepend, 0, returnByte, 0, Prepend.Length);
            Buffer.BlockCopy(Message, 0, returnByte, Prepend.Length, Message.Length);
            return returnByte;
        }

        // Properties
        public bool IsConnected
        {
            get
            {
                if (_tcpClient != null)
                    return _tcpClient.Connected;
                else
                    return false;
            }
        }
        public IrcServerData IrcServerData
        {
            get
            {
                return _ircClientData;
            }
        }

        public event MessageEventHandler MessageEvent
        {
            add
            {
                _messageEvent += value;
            }
            remove
            {
                _messageEvent -= value;
            }
        }
    }
}
