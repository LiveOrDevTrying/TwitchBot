﻿namespace TwitchBot.IrcClient
{
    public interface IIrcClient
    {
        IrcServerData IrcServerData { get; }
        bool IsConnected { get; }

        event MessageEventHandler MessageEvent;

        void Connect(string userName, string password, string serverName, int port, bool hasOutgoingTimer, float messageRate);
        void Disconnect();
        void SendMessage(string message);
    }
}