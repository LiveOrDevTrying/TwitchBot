﻿using System.Collections.Generic;
using TwitchBot.Enum;
using TwitchBot.IrcClient;
using TwitchBot.Model;

namespace TwitchBot
{
    public interface ITwitchIrcClient
    {
        BotState BotState { get; }
        List<TwitchChannel> Channels { get; }
        IIrcClient IrcClient { get; }
        IrcServerData IrcServerData { get; }
        bool IsConnected { get; }

        event StateChangeEventHandler BotStateChange;
        event ChannelEventHandler ChannelEvent;
        event Model.MessageEventHandler MessageEvent;

        void ChangeTextColor(TwitchChannel twitchChannel, ChatTextColor chatTextColor);
        void ConnectToTwitch(string userName, string password);
        TwitchChannel ConnectToTwitchChannel(string channelName);
        void DisconnectFromTwitch();
        void DisconnectFromTwitchChannel(string channelName);
        void Dispose();
        void SendAction(TwitchChannel twitchChannel, string message);
        void SendChatMessage(TwitchChannel twitchChannel, string message);
        void SendMessage(string message);
        void SendWhisper(TwitchChannel twitchChannel, string message, string userName);
    }
}