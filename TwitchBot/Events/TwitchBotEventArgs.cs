﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchBot.Enum;

namespace TwitchBot.Events
{
    public class TwitchBotEventArgs : EventArgs
    {
        public BotState BotState { get; set; }
    }
}
