﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchBot.Events
{
    public class IrcMessageEventArgs : EventArgs
    {
        public string MessageRaw { get; set; }
        public SendOrReceive SendOrReceive { get; set; }
    }

    public enum SendOrReceive
    {
        Send,
        Receive
    }
}
