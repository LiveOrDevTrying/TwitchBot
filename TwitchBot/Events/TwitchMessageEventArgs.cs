﻿using TwitchBot.Model;

namespace TwitchBot.Events
{
    public class TwitchMessageEventArgs : EventArgs
    {
        public TwitchMessage TwitchMessage { get; set; }
    }
}
