﻿using TwitchBot.Model;

namespace TwitchBot.Events
{
    public class TwitchUserEventArgs : EventArgs
    {
        public TwitchUser TwitchUser { get; set; }
        public MessageType MessageType { get; set; }
    }
}
