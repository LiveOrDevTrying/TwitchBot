﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitchBot.Model;

namespace TwitchBot.Events
{
    public class TwitchChannelEventArgs : EventArgs
    {
        public TwitchChannel TwitchChannel { get; set; }
        public LoginType LoginType { get; set; }
    }
}
