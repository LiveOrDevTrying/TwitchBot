﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitchBot.Enum
{ 
    public enum BotState
    {
        Idle,
        Connecting,
        Connected
    }

    public enum ChatTextColor
    {
        Blue,
        Green,
        Purple,
        Yellow,
        Red
    }
}
