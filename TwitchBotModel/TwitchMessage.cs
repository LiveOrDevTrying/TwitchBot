﻿namespace TwitchBotModel
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Serializable]
    public partial class TwitchMessage
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int? Channel_ID { get; set; }

        public Guid? User_ID { get; set; }

        public MessageType MessageType { get; set; }

        public string MessageFormatted { get; set; }

        public string MessageRaw { get; set; }

        public DateTime TimeStamp { get; set; }

        public virtual TwitchChannel Channel { get; set; }

        public virtual TwitchUser User { get; set; }
    }

    public enum MessageType
    {
        Join,
        Leave,
        Chat,
        Whisper,
        Notice,
        Other
    }
}
