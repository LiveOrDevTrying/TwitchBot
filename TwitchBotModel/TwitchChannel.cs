﻿namespace TwitchBotModel
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Serializable]
    public partial class TwitchChannel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string ChannelName { get; set; }

        public bool IsAutoJoin { get; set; }

        public bool IsUserOnline { get; set; }

        public bool IsAssignedServer { get; set; }

        public bool Active { get; set; }

        public DateTime TimeStampCreated { get; set; }

        public DateTime TimeStampModified { get; set; }
    }
}
