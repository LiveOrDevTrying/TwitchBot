﻿namespace TwitchIRCClient
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Net.Sockets;
    using System.Timers;
    using IRCClient;

    public delegate void IrcMessageReceivedEventHandler(string messageRaw);

    public class TwitchIrcClient : IDisposable
    {
        private IIrcClient _ircClient;
        private IrcMessageReceivedEventHandler _receivedMessage;

        public TwitchIrcClient()
        {
            _ircClient = new IrcClient();
            _ircClient.MessageReceived += OnMessageReceived;
        }

        public void ConnectToTwitch(string userName, string password, string serverName, int port)
        {
            _ircClient.Connect(userName, password, serverName, port, true, MessageRate());

            Command_SendCommandCapRequest();
            Command_SendCommandCapRequest();
        }
        public void DisconnectFromTwitch()
        {
            if (_ircClient.IsConnected)
            {
                _ircClient.Disconnect();
            }
        }
        public void SendMessage(string message)
        {
            _ircClient.SendMessage(message);
        }

        private void OnMessageReceived(string ircMessageRaw, SendOrReceived sendOrReceived)
        {
            if (_receivedMessage != null)
            {
                _receivedMessage(ircMessageRaw);
            }
        }

        private float MessageRate()
        {
            return (1000f * 20f) / (30f * .9f);       // 20 messages in 30 sec if not in milliseconds with 10% buffer
        }
        private void PingPong()
        {
            SendMessage("PONG :tmi.twitch.tv");
        }
        private void Command_SendMembershipCapRequest()
        {
            // Send cap request between joining server and channel to track new followers
            // Courtesy of Devin_Brimer
            string _capRequest = "CAP REQ :twitch.tv/membership";
            SendMessage(_capRequest);
        }
        private void Command_SendCommandCapRequest()
        {
            string _capRequest = "CAP REQ :twitch.tv/commands";
            SendMessage(_capRequest);
        }

        public void Dispose()
        {
            if (_ircClient != null)
            {
                _ircClient.MessageReceived -= OnMessageReceived;
            }
        }

        // Properties
        public bool IsConnected
        {
            get
            {
                return _ircClient.IsConnected;
            }
        }
        public IrcClientData DataIrcClient
        {
            get
            {
                return _ircClient.DataIrcClient;
            }
        }
      
        public event IrcMessageReceivedEventHandler MessageReceived
        {
            add
            {
                _receivedMessage += value;
            }
            remove
            {
                _receivedMessage -= value;
            }
        }
    }
}
